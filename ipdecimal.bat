cls
@echo off
color 2
echo.
echo                             ===============
echo                              ip en decimal
echo                             ===============
echo.
set /p per=" 1er nombre:"
set /p deme=" deuxieme:"
set /p teme=" troisieme:"
set /p qeme=" quatrieme:"
set /a somp=%per%*16777216
set /a somd=%deme%*65536
set /a somt=%teme%*256
set /a dec=%somp%+%somd%+%somt%+%qeme%
echo adresse en decimal: %dec%
pause>nul